import { required, length } from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("required", {
  ...required,
  message: "Không được để trống."
});

extend("length", {
  ...length,
  message: "Thứ tự hiển thị phải lớn hơn 0."
});

extend("positive", {
    validate: (val) => val > 0,
    message: "Thứ tự hiển thị phải lớn hơn 0."
});
